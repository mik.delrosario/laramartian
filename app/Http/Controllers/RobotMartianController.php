<?php

namespace App\Http\Controllers;

use App\Services\Landarea;

class RobotMartianController extends Controller
{
    public function index()
    {
        try {
            $index = 0;
            $inputFile = fopen(storage_path("input.txt"), "r");

            echo "### SAMPLE INPUT<br/><br/>";
            while (!feof($inputFile)) {
                $input = fgets($inputFile);
                echo $input . " <br/>";
                if ($index == 0) {
                    $str_arr = explode(" ", $input);
                    if(((int)$str_arr[0]) <= 50 && ((int)$str_arr[1]) <= 50){
                        $landarea = new Landarea($str_arr[0], $str_arr[1]);
                    }else{
                        die('Error Coordinates Greater than 50');
                    }
                } else if ($index % 2 != 0) {
                    $str_arr = explode(" ", $input);
                    if(((int)$str_arr[0]) <= 50 && ((int)$str_arr[1]) <= 50){
                        $landarea->setRobots($str_arr[0], $str_arr[1], $str_arr[2]);
                    }else{
                        die('Error Coordinates Greater than 50');
                    }
                } else {
                    if(strlen(trim($input)) < 100){
                        $landarea->setRobotInstructions($input);
                        $landarea->calculatePosition();
                    }else{
                        die('Error instructions Greater than equal to 100');
                    }
                    
                }

                $index++;
            }
            fclose($inputFile);

            echo "<br/>### SAMPLE OUTPUT<br/><br/>";
            foreach ($landarea->getRobotsReport() as $key => $value) {
                echo $value."<br/>";
            }

        } catch (Illuminate\Contracts\Filesystem\FileNotFoundException $exception) {
            die("The file doesn't exist");
        }
    }
}
