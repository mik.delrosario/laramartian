<?php

namespace App\Services;

use App\Services\Robot;
use App\Services\Movement;

class Landarea
{
    public $upperPos = 0;
    public $rightPos = 0;
    public $lowerPos = 0;
    public $leftPos = 0;

    public $index = -1;

    public $array_of_robots = [];

    public function __construct($rightPos, $upperPos)
    {
        $this->upperPos = $upperPos;
        $this->rightPos = $rightPos;
    }

    public function setRobots($positionX, $positionY, $facing)
    {
        $this->index++;
        $this->array_of_robots[$this->index] = new Robot($positionX, $positionY, $facing);
    }

    public function setRobotInstructions($instructions)
    {
        $this->array_of_robots[$this->index]->setInstructions($instructions);
    }

    public function checkRobotLost()
    {
        $tempRobot = $this->array_of_robots[$this->index];
        $minX = ($this->leftPos < $this->rightPos ? $this->leftPos : $this->rightPos);
        $maxX = ($this->leftPos > $this->rightPos ? $this->leftPos : $this->rightPos);
        $minY = ($this->lowerPos < $this->upperPos ? $this->lowerPos : $this->upperPos);
        $maxY = ($this->lowerPos > $this->upperPos ? $this->lowerPos : $this->upperPos);

        if ($tempRobot->getPositionX() > $maxX ||
            $tempRobot->getPositionX() < $minX ||
            $tempRobot->getPositionY() > $maxY ||
            $tempRobot->getPositionY() < $minY) {
            $this->array_of_robots[$this->index]->setLostLocation(true);
        }
        return $this->array_of_robots[$this->index]->getLostLocation();
    }

    public function calculatePosition()
    {
        $tempRobot = $this->array_of_robots[$this->index];
        $tempInstruction = $tempRobot->getInstructions();

        for ($x = 0; $x < strlen($tempInstruction); $x++) {
            if (trim($tempInstruction[$x]) == Movement::MOVE_FORWARD) {
                $this->array_of_robots[$this->index]->forward();
                if ($this->checkRobotLost()) {
                    $this->array_of_robots[$this->index]->lastPosition();
                    break;
                }
            } else if (trim($tempInstruction[$x]) == Movement::RIGHT ||
                trim($tempInstruction[$x]) == Movement::LEFT) {
                $this->array_of_robots[$this->index]->changeDirection(trim($tempInstruction[$x]));
            }
        }
    }

    public function getRobotsReport()
    {
        $array_of_reports = array();

        for ($x = 0; $x < count($this->array_of_robots); $x++) {
            $tempRobot = $this->array_of_robots[$x];
            $array_of_reports[] = $tempRobot->getPositionX() . " "
                . $tempRobot->getPositionY() . " "
                . $tempRobot->getFacing() . " "
                . ($tempRobot->getLostLocation() ? "LOST" : "");
        }

        return $array_of_reports;
    }
}
