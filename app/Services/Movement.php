<?php

namespace App\Services;

class Movement
{

    public const LEFT_MOVEMENT = 'L';
    public const RIGHT_MOVEMENT = 'R';
    public const MOVE_FORWARD = 'F';

    public const NORTH = 'N';
    public const EAST = 'E';
    public const SOUTH = 'S';
    public const WEST = 'W';

    public const RIGHT = 'R';
    public const LEFT = 'L';

    public const OUT_OF_BOUNDS = 'LOST';

    public $step = 1;
    public $facing;
    public $instructions;

    public function setFacing($facing)
    {
        $this->facing = $facing;
    }

    public function getFacing()
    {
        return $this->facing;
    }

    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;
    }

    public function getInstructions()
    {
        return $this->instructions;
    }

    public function changeDirection($faceTo)
    {
        $directions = array(0 => self::NORTH, 1 => self::EAST, 2 => self::SOUTH, 3 => self::WEST);
        $currentIndex = 0;
        foreach ($directions as $key => $value) {
            if (trim($value) == trim($this->facing)) {
                $currentIndex = $key;
            }
        }

        if ($faceTo == self::RIGHT) {
            $currentIndex++;
        } else if ($faceTo == self::LEFT) {
            $currentIndex--;
        }

        if ($currentIndex < 0) {
            $currentIndex = count($directions) - 1;
        } else if ($currentIndex >= count($directions)) {
            $currentIndex = 0;
        }
        
        $this->setFacing($directions[$currentIndex]);
    }
}
