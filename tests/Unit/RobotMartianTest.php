<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Services\Landarea;

class RobotMartianTest extends TestCase
{
    public function testDefaultLandArea()
    {
        $landarea = new Landarea(5, 3);
        $this->assertEquals($landarea->upperPos, 3);
        $this->assertEquals($landarea->rightPos, 5);
        $this->assertEquals($landarea->lowerPos, 0);
        $this->assertEquals($landarea->leftPos, 0);
    }

    public function testRobotMovement()
    {
        $landarea = new Landarea(5,3);
        $landarea->setRobots(0,3,"W");
        $landarea->setRobotInstructions("LLFFFLFLFL");
        $landarea->calculatePosition();

        $this->assertEquals($landarea->getRobotsReport(), ["2 3 S"]);
    }
}
