<?php

namespace App\Services;

use App\Services\Movement;

class Robot extends Movement
{

    public $positionX = 0;
    public $positionY = 0;
    public $lost = false;
    public $instructions;

    public function __construct($positionX, $positionY, $facing)
    {
        $this->positionX = $positionX;
        $this->positionY = $positionY;
        $this->setFacing($facing);
    }

    public function getPositionX()
    {
        return $this->positionX;
    }

    public function getPositionY()
    {
        return $this->positionY;
    }

    public function setLostLocation($lost)
    {
        $this->lost = $lost;
    }

    public function getLostLocation()
    {
        return $this->lost;
    }

    public function forward()
    {
        switch (trim($this->getFacing())) {
            case self::NORTH:
                $this->positionY += 1;
                break;

            case self::EAST:
                $this->positionX += 1;
                break;

            case self::SOUTH:
                $this->positionY -= 1;
                break;

            case self::WEST:
                $this->positionX -= 1;
                break;

            default:
                # code...
                break;
        }
    }

    public function lastPosition()
    {
        switch (trim($this->getFacing())) {
            case self::NORTH:
                $this->positionY -= 1;
                break;

            case self::EAST:
                $this->positionX -= 1;
                break;

            case self::SOUTH:
                $this->positionY += 1;
                break;

            case self::WEST:
                $this->positionX += 1;
                break;

            default:
                # code...
                break;
        }
    }
}
